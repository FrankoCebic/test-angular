var express = require('express');
var app = express();
var bodyParser  = require('body-parser');
var connect = require("connect");
var appp = connect.createServer().use(connect.static(__dirname + '/public'));

app.post('/', function (req, res){
  res.send('Hello World!')
});

app.listen(3000, console.log("Listening on 3000"));